﻿App.Router.map(function () {
    this.resource('users', { path: '/' });
    this.resource('user.chat', { path: '/chat/:nick' });
    this.resource('archiv', { path: '/archiv' });
    this.resource('settings', { path: '/settings' });
});

App.ApplicationRoute = Ember.Route.extend({
    actions: {
    }
});

App.IndexRoute = Ember.Route.extend({
    beforeModel: function (transition) {
        this.transitionTo('users');
    }
});