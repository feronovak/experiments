﻿// log binding activities
Ember.LOG_BINDINGS = true;

window.App = Ember.Application.create({
    LOG_ACTIVE_GENERATION: true,
    LOG_VIEW_LOOKUPS: true,
    rootElement: '#placeholder'
});
