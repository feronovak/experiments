﻿App.Contact = DS.Model.extend({
    id: DS.attr('int'),
    nick: DS.attr('string'),
    photo: DS.attr('string')
});

App.Contact.FIXTURES = [
    {
        id: 1,
        nick: 'scooby',
        photo: ''
    },
    {
        id: 2,
        nick: 'maros',
        photo: ''
    },
    {
        id: 3,
        nick: 'speto',
        photo: ''
    },
];