﻿<%@ Page Title="Pokec Ember + Bootstrap mashup" Language="C#" MasterPageFile="~/Template.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="chat._default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid height-full">
        <div class="row height-full">
            <div class="col-xs-1 col-md-2 height-full col-services">
                <ul class="list-unstyled">
                    <li><a href="/#/"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><span class="hidden-xs hidden-sm">Rychla posta</span></a></li>
                    <li><a href="/#/archiv"><span class="glyphicon glyphicon-hdd" aria-hidden="true"></span><span class="hidden-xs hidden-sm">Archiv RP</span></a></li>
                    <li><a href="/#/settings"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span><span class="hidden-xs hidden-sm">Nastavenia</span></a></li>
                </ul>
            </div>
            <div id="placeholder"></div>
        </div>
    </div>

    <script type="text/x-handlebars"  data-template-name="settings">
        <div class="col-xs-11 col-md-10 height-full col-settings">
            <h3>Nastavenia</h3>
            <div class="checkbox">
              <label>
                <input type="checkbox" value="">
                Option one is this and that&mdash;be sure to include why it's great
              </label>
            </div>
            <div class="checkbox disabled">
              <label>
                <input type="checkbox" value="" disabled>
                Option two is disabled
              </label>
            </div>
        </div>
    </script>
    <script type="text/x-handlebars"  data-template-name="users">
        <div class="col-xs-3 col-md-2 height-full col-list">
            <ul class="list-unstyled" id="chat-list">
                <li><a href=""><img src="http://m2.aimg.sk/profil/s_415181.jpg?v=21" class="img-circle" style="margin-right:10px;">scooby <span class="badge pull-right">4</span></a></li>
                <li><a href=""><img src="http://m1.aimg.sk/profil/s_24891488.jpg?v=7" class="img-circle" style="margin-right:10px;">maros</a></li>
                <li><a href=""><img src="http://m2.aimg.sk/profil/s_24471357.jpg?v=11" class="img-circle" style="margin-right:10px;">stevo</a></li>
                <li><a href=""><img src="http://m4.aimg.sk/profil/s_20384251.jpg?v=3" class="img-circle" style="margin-right:10px;">if</a></li>
            </ul>
        </div>
        <div class="col-xs-8 col-md-8 height-full col-msgs">
            <div class="box-msgs">
                <div class="msg msg-opponent">
                    <h5><strong>scooby</strong> <span>10:11</span></h5>
                    <div>
                        <p>Do you see any Teletubbies in here? Do you see a slender plastic tag clipped to my shirt with my name printed on it? Do you see a little Asian child with a blank expression on his face sitting outside on a mechanical helicopter that shakes when you put quarters in it? No? Well, that's what you see at a toy store. And you must think you're in a toy store, because you're here shopping for an infant named Jeb. </p>
                    </div>
                </div>
                <div class="msg msg-opponent">
                    <h5><strong>scooby</strong> <span>8:25</span></h5>
                    <div>
                        <p>Well, the way they make shows is, they make one show. That show's called a pilot. Then they show that show to the people who make shows, and on the strength of that one show they decide if they're going to make more shows. Some pilots get picked and become television programs. Some don't, become nothing. She starred in one of the ones that became nothing. </p>
                        <p>Look, just because I don't be givin' no man a foot massage don't make it right for Marsellus to throw Antwone into a glass motherfuckin' house, fuckin' up the way the nigger talks. Motherfucker do that shit to me, he better paralyze my ass, 'cause I'll kill the motherfucker, know what I'm sayin'? </p>
                    </div>
                </div>
                <div class="msg msg-me">
                    <h5><strong>fero</strong> <span>včera</span></h5>
                    <div>
                        <p>You think water moves fast? You should see ice. It moves like it has a mind. Like it knows it killed the world once and got a taste for murder. After the avalanche, it took us a week to climb out. Now, I don't know exactly when we turned on each other, but I know that seven of us survived the slide... and only five made it out. Now we took an oath, that I'm breaking now. We said we'd say it was the snow that killed the other two, but it wasn't. Nature is lethal but it doesn't hold a candle to man. </p>
                        <p>Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass. </p>
                    </div>
                </div>
            </div>
            <div class="box-send col-md-7">
                <div class="col-xs-10 msg-input">
                    <input type="text" class="form-control " placeholder="Tvoja správa">
                </div>
                <div class="col-xs-2 msg-input">
                    <button type="button" class="form-control btn btn-success"><span class="glyphicon glyphicon-send" aria-hidden="true"></span><span class="hidden-xs hidden-sm">odoslať</span></button>
                </div>
            </div>
        </div>
    </script>

    <script type="text/x-handlebars"  data-template-name="archiv">
        <div class="col-xs-4 col-md-3 height-full col-list">
            <ul class="list-unstyled" id="chat-list">
                <li>
                    <a href=""><img src="http://m2.aimg.sk/profil/s_415181.jpg?v=21" class="img-circle" style="margin-right:10px; float: left; margin-top: 4px;">
                        <span>scooby</span>
                        <p>22 správ, posledná správa včera</p>
                    </a>
                </li>
                <li>
                    <a href=""><img src="http://m1.aimg.sk/profil/s_24891488.jpg?v=7" class="img-circle" style="margin-right:10px; float: left; margin-top: 4px;">
                        <span>maros</span>
                        <p>85 správ, posledná správa 31. januára</p>
                    </a>
                </li>
                <li>
                    <a href=""><img src="http://m2.aimg.sk/profil/s_24471357.jpg?v=11" class="img-circle" style="margin-right:10px; float: left; margin-top: 4px;">
                        <span>stevo</span>
                        <p>105 správ, posledná správa 2. januára</p>
                    </a>
                </li>
                <li>
                    <a href=""><img src="http://m4.aimg.sk/profil/s_25929303.jpg?v=1" class="img-circle" style="margin-right:10px; float: left; margin-top: 4px;">
                        <span>malinka</span>
                        <p>9 správ, posledná správa 8. januára</p>
                    </a>
                </li>
                <li>
                    <a href=""><img src="http://m4.aimg.sk/profil/s_20384251.jpg?v=3" class="img-circle" style="margin-right:10px; float: left; margin-top: 4px;">
                        <span>if</span>
                        <p>105 správ, posledná správa 2. januára</p>
                    </a>
                </li>
                <li>
                    <a href=""><img src="http://m1.aimg.sk/profil/s_20039696.jpg?v=8" class="img-circle" style="margin-right:10px; float: left; margin-top: 4px;">
                        <span>tonyfx</span>
                        <p>9992 správ, posledná správa 24. 12. 2014</p>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-xs-7 col-md-7 height-full col-msgs">
            <div class="box-msgs">
                <div class="msg msg-opponent">
                    <h5><strong>scooby</strong> <span>10:11</span></h5>
                    <div>
                        <p>Do you see any Teletubbies in here? Do you see a slender plastic tag clipped to my shirt with my name printed on it? Do you see a little Asian child with a blank expression on his face sitting outside on a mechanical helicopter that shakes when you put quarters in it? No? Well, that's what you see at a toy store. And you must think you're in a toy store, because you're here shopping for an infant named Jeb. </p>
                    </div>
                </div>
                <div class="msg msg-opponent">
                    <h5><strong>scooby</strong> <span>8:25</span></h5>
                    <div>
                        <p>Well, the way they make shows is, they make one show. That show's called a pilot. Then they show that show to the people who make shows, and on the strength of that one show they decide if they're going to make more shows. Some pilots get picked and become television programs. Some don't, become nothing. She starred in one of the ones that became nothing. </p>
                        <p>Look, just because I don't be givin' no man a foot massage don't make it right for Marsellus to throw Antwone into a glass motherfuckin' house, fuckin' up the way the nigger talks. Motherfucker do that shit to me, he better paralyze my ass, 'cause I'll kill the motherfucker, know what I'm sayin'? </p>
                    </div>
                </div>
                <div class="msg msg-me">
                    <h5><strong>fero</strong> <span>včera</span></h5>
                    <div>
                        <p>You think water moves fast? You should see ice. It moves like it has a mind. Like it knows it killed the world once and got a taste for murder. After the avalanche, it took us a week to climb out. Now, I don't know exactly when we turned on each other, but I know that seven of us survived the slide... and only five made it out. Now we took an oath, that I'm breaking now. We said we'd say it was the snow that killed the other two, but it wasn't. Nature is lethal but it doesn't hold a candle to man. </p>
                        <p>Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass. </p>
                    </div>
                </div>
            </div>
            <div class="box-send col-md-7">
                <div class="col-xs-10 msg-input">
                    <input type="text" class="form-control " placeholder="Tvoja správa">
                </div>
                <div class="col-xs-2 msg-input">
                    <button type="button" class="form-control btn btn-success"><span class="glyphicon glyphicon-send" aria-hidden="true"></span><span class="hidden-xs hidden-sm">odoslať</span></button>
                </div>
            </div>
        </div>
    </script>
</asp:Content>